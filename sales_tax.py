# --- Aisle Sales Tax Problem --- #

# -- importing modules -- #

import csv
from math import ceil

# --- Class representing Items in Basket --- #


class Item:

    # --- Class variables for tax calculations --- #

    bst_rate = 10
    imt_rate = 5
    tax_rounding = 0.05

    # --- List of tax exempt items --- #

    tax_exempts = ["book", "chocolate", "pill"]
    item_list = []

    # --- Item constructor --- #

    def __init__(self, quantity: int, name: str, price: float):

        # --- Assertion statements to avoid errors --- #

        assert quantity > 0, "Quantity needs to be greater than 0"
        assert price > 0, "Price needs to be greater than 0"

        # --- Assign instance variables --- #

        self.quantity = quantity
        self.name = name
        self.price = price

        Item.item_list.append(self)

    def __repr__(self):
        return f"Item({self.quantity}, '{self.name}', {self.price})"

    # --- Class method to read data from a csv --- #

    @classmethod
    def read_from_csv(cls, path):
        Item.item_list = []
        with open(path, 'r') as f:
            reader = csv.DictReader(f)
            items = list(reader)
            for item in items:
                Item(int(item['quantity']),
                     item['name'],
                     float(item['price']))

    # --- Method to calculate tax of an item --- #

    def get_tax(self):
        rate = 0
        for name in Item.tax_exempts:
            if name in self.name:
                break
        else:
            rate = self.bst_rate
        if "import" in self.name:
            rate += self.imt_rate
        tax = (self.quantity * self.price) * rate/100
        return ceil(tax/self.tax_rounding) * self.tax_rounding

    # --- Class method to show all items in list with the corresponding output --- #

    @classmethod
    def show_items(cls):
        tax = 0
        total = 0
        for item in cls.item_list:
            tax += item.get_tax()
            item_total = item.price + item.get_tax()
            total += item_total
            print(f"{item.quantity} {item.name}: {item_total:.2f}")
        print(f"Sales taxes: {tax:.2f} Total: {total:.2f}")

# --- Paths of receipts --- #


paths = ['receipt_a.csv', 'receipt_b.csv', 'receipt_c.csv' ]

for path in paths:
    print(path)
    Item.read_from_csv(path)
    Item.show_items()
    print()